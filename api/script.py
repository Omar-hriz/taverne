import tkinter as tk
from tkinter import messagebox

class XPTrackerApp:
    def __init__(self, master):
        self.master = master
        self.master.title("XP Tracker")
        self.master.geometry("500x300")

        self.xp_label = tk.Label(master, text="Entrez le montant d'XP :", font=("Helvetica", 14))
        self.xp_label.pack(pady=10)

        self.xp_entry = tk.Entry(master, font=("Helvetica", 12))
        self.xp_entry.pack(pady=10)

        self.submit_button = tk.Button(master, text="Soumettre", command=self.submit_xp, font=("Helvetica", 12, "bold"))
        self.submit_button.pack()

        self.niveau_label = tk.Label(master, text="Niveau actuel : 0", font=("Helvetica", 12))
        self.niveau_label.pack(pady=5)

        self.result_label = tk.Label(master, text="Entrez le montant d'XP pour commencer", font=("Helvetica", 12, "bold"))
        self.result_label.pack(pady=10)

        # Barre de menu
        menu_bar = tk.Menu(master)
        master.config(menu=menu_bar)

        file_menu = tk.Menu(menu_bar, tearoff=0)
        menu_bar.add_cascade(label="Options", menu=file_menu)
        file_menu.add_command(label="Réinitialiser le niveau", command=self.reset_niveau)

    def lire_valeur(self):
        try:
            with open('var.txt', 'r') as f:
                valeur = int(f.read())
        except FileNotFoundError:
            valeur = 0
        return valeur

    def write_valeur(self, xp_cumule):
        with open('var.txt', 'w') as f:
            f.write(str(xp_cumule))

    def calculer_niveau(self, xp_cumule):
        somme = 0
        for i in range(len(niveau)):
            somme += niveau[i]
            if xp_cumule < somme:
                return i, max(0, somme - xp_cumule), niveau[i]

        return len(niveau) - 1, 0, niveau[-1]

    def reset_niveau(self):
        confirmation = messagebox.askyesno("Réinitialiser le niveau", "Êtes-vous sûr de vouloir réinitialiser le niveau?")
        if confirmation:
            self.write_valeur(0)
            self.update_labels(0, "Niveau réinitialisé à 0\nEntrez le montant d'XP pour commencer")

    def submit_xp(self):
        try:
            xp_saisi = int(self.xp_entry.get())
        except ValueError:
            self.update_labels(0, "Veuillez entrer un nombre valide", clear_entry=True)
            return

        xp_cumule = self.lire_valeur() + xp_saisi
        niveau, xp_restant, prochain_niveau = self.calculer_niveau(xp_cumule)
        self.update_labels(niveau, f"Vous avez {prochain_niveau - xp_restant} XP\nEncore {xp_restant} XP avant le prochain niveau")
        self.write_valeur(xp_cumule)

    def update_labels(self, niveau, result_text, clear_entry=False):
        self.niveau_label.config(text=f"Niveau actuel : {niveau}")
        self.result_label.config(text=result_text)
        if clear_entry:
            self.xp_entry.delete(0, 'end')

if __name__ == "__main__":
    niveau = [5, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55]
    root = tk.Tk()
    app = XPTrackerApp(root)
    root.mainloop()
